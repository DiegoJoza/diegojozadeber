package facci.diegojoza.appproyecto;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
  Button convertir,convertir2;
  EditText cel, far;
  Double f=1.8,C=0.55;
  TextView resultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        convertir2 = findViewById(R.id.convertir2);
        convertir = findViewById(R.id.convertir1);
        cel = findViewById(R.id.cel);
        far = findViewById(R.id.faren);
        resultado = findViewById(R.id.resultado);
        convertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double temp = Double.parseDouble(cel.getText().toString());
                Double tempfn = temp * f + 32;
                resultado.setText( "" + tempfn +"F");
            }
        });
        convertir2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double temp1 = Double.parseDouble(far.getText().toString());
                Double tempfn1 = temp1 * C - 32;
                resultado.setText( "" + tempfn1 +"C");
            }
        });
        Log.i("Programacion Movil", "Diego Alejandro Joza Holguin");
    }
}
